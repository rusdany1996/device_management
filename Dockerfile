FROM openjdk:8
ADD build/libs/device-management-0.0.1-SNAPSHOT.jar device-management-0.0.1-SNAPSHOT.jar
EXPOSE 8083:8083
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","device-management-0.0.1-SNAPSHOT.jar"]