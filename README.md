# Device_Management

Scurta descriere a aplicatiei:
Prin aceasta aplicatie utilizatorul poate sa verifice dispozitivele pe care le are la dispozitie, dar si sa faca upload la fisiere ca dupa aceea sa le poata descarca.


Aceasta este schema mea pentru baza de date si toate tabelele pe care sa le cuprinda:
![image info](/home/daniel/Documents/device_management/device-management_edit.jpg "Database Schema")
Intre tabela de User si tabela de Device este o relatie many to many pe care am transformat-o intr-o relatie de one to many prin adaugarea tabelei User_Device. Coloanele user_id si device_id formeaza impreauna PK care la randul sunt FK pentru tabelele respective.

# Patterns
In aplicatie este implementat Design Patternul Factory pentru a creea serviciile necesare in controllere, si Design Patternul Singleton facut in spate de SpringBoot pentru conexiunea la baza de date.

In aplicatie este Observer pattern este implementat doar pentru a afisa mesaje de success sau fail.

In aceasta aplicatie sunt 2 tipuri de utilizatori ADMIN-ul care are toate permisiunile, USERUL care poate si sa faca uplaoda dar si sa aiba in posesie diviceuri.

# Tehnologii
Lista de tehnologii folosite:
* SpringBoot MVC
* React
* Docker
* Yarn
* npm

Pasii pe care trebuie sa ii urmezi ca sa faci aplicatia sa mearga:
1. Trebuie sa fie instalate urmatoarele tehnologii:
* Docker 
* Yarn
* npm
* Idea
2. Clone repository
3. In folderul de priect 
```
yarn create react-app app
cd app
yarn add bootstrap@4.1.3 react-cookie@3.0.4 react-router-dom@4.3.1 reactstrap@6.5.0
```
4. Copiaza tot ce este in my-app/src in app/src si 
```
yarn start
```
5. Pentru a porni serverul 
```
./build.sh
```
Pentru a porni back-endul si serverul.
Pentru ca baza de date din conrainerul de docker sa poata comunica cu serverul trebuie asaugat in 
```
cd /etc
echo "127.0.0.1 device_management" > hosts
```
6. Frontend-ul este activ pe localhost:3000/api/

# Clase

In aplicatie sunt perezente mai multe clase:
* User care este cel care utilizeaza aplicatia.
* Token pentru a se verifica autenticitatea utilizatorului pentru fiecara call pe care il face.
* File fisierele salvate in app.
* Device sunt dispozitivele pe care le detine un utilizator.
* Role pentru a tine rolul fiecarui user.



