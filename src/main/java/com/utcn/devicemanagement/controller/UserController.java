package com.utcn.devicemanagement.controller;

import com.utcn.devicemanagement.model.*;
import com.utcn.devicemanagement.repository.TokenRepository;
import com.utcn.devicemanagement.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController {

    private UserRepository userRepository;
    private TokenRepository tokenRepository;
    private final Logger log = LoggerFactory.getLogger(UserController.class);

    public UserController(UserRepository userRepository, TokenRepository tokenRepository){
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
    }


    @GetMapping("/users")
    ResponseEntity<?> getUsers(@RequestHeader("accessToken") String accessToken)  {
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            log.info("Request to get users: {}", userRepository.findAll());
            return  ResponseEntity.status(HttpStatus.OK)
                    .body(new Response<User>().builder()
                            .accessToken(token.getAccessToken())
                            .payloads(new ArrayList<>(userRepository.findAll()))
                            .build());
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }


    @PostMapping("/user")
    ResponseEntity<?> getUser(@Valid @RequestBody RequestUser request,@RequestHeader("accessToken") String accessToken) {
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            log.info("Request to get user: {}", request.getUsername());
            User result = userRepository.findUserByUsername(request.getUsername());
            if(result != null){
                return  ResponseEntity.status(HttpStatus.OK)
                        .body(new Response<User>().builder()
                                .payload(result)
                                .build());
            }
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }





}