package com.utcn.devicemanagement.controller;

import com.utcn.devicemanagement.model.*;
import com.utcn.devicemanagement.repository.DeviceRepository;
import com.utcn.devicemanagement.repository.TokenRepository;
import com.utcn.devicemanagement.repository.UserRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class DeviceController {

    private DeviceRepository deviceRepository;
    private TokenRepository tokenRepository;
    private UserRepository userRepository;
    private final Logger log = LoggerFactory.getLogger(DeviceController.class);

    public DeviceController(DeviceRepository deviceRepository, TokenRepository tokenRepository,UserRepository userRepository) {

        this.deviceRepository = deviceRepository;
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/devices")
    ResponseEntity<?> getDevices( @RequestHeader("accessToken") String accessToken,@RequestHeader("user") String username) {
        log.info("Request to verify token : {}",accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            User user = userRepository.findUserByUsername(username);
            log.info("Request to get user : {}", username);
            if(user.getRole().getId() == 1){
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new Response<Device>().builder()
                                .payloads(new ArrayList<>(deviceRepository.findAll()))
                                .build());
            }
            else{
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new Response<Device>().builder()
                                .payloads(new ArrayList<>(deviceRepository.findDevicesByUser(user)))
                                .build());
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/device/{id}")
    ResponseEntity<?> getDevice(@PathVariable Long id, @RequestHeader("accessToken") String accessToken) {
        log.info("Request to verify token : {}",accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            Optional<Device> device = deviceRepository.findById(id);
            log.info("Request to get device : {}", device);
            return device.map(response -> ResponseEntity.ok().body(response))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/device")
    ResponseEntity<Device> createGroup(@Valid @RequestBody RequestDevice device,@RequestHeader("accessToken") String accessToken) throws URISyntaxException {
        log.info("Request to verify token : {}",accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            log.info("Request to create device: {}", device);
            User user = userRepository.findUserByUsername(device.getUser());
            Device result = deviceRepository.save(new Device().builder()
                    .phoneCompany(device.getPhoneCompany())
                    .serialNo(device.getSerialNo())
                    .macAddress(device.getMacAddress())
                    .ram(device.getRam())
                    .processor(device.getProcessor())
                    .user(user)
                    .build());
            return ResponseEntity.created(new URI("/api/group/" + result.getId()))
                    .body(result);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PutMapping("/device")
    ResponseEntity<?> updateGroup(@Valid @RequestBody RequestDevice device,@RequestHeader("accessToken") String accessToken) {
        log.info("Request to verify token : {}",accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null) {
            log.info("Request to update device: {}", device);
            User user = userRepository.findUserByUsername(device.getUser());
            Device result = deviceRepository.findDeviceById(device.getId());
            result.setMacAddress(device.getMacAddress());
            result.setPhoneCompany(device.getPhoneCompany());
            result.setSerialNo(device.getSerialNo());
            result.setRam(device.getRam());
            result.setProcessor(device.getProcessor());
            result.setUser(user);
            deviceRepository.save(result);
            return ResponseEntity.ok().body(result);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
