package com.utcn.devicemanagement.controller;

import com.utcn.devicemanagement.model.*;
import com.utcn.devicemanagement.repository.RoleRepository;
import com.utcn.devicemanagement.repository.TokenRepository;
import com.utcn.devicemanagement.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Random;


@RestController
@RequestMapping("/api")
public class AuthController {

    private UserRepository userRepository;
    private TokenRepository tokenRepository;
    private RoleRepository roleRepository;

    private final Logger log = LoggerFactory.getLogger(AuthController.class);

    public AuthController(UserRepository userRepository, TokenRepository tokenRepository, RoleRepository roleRepository){
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.roleRepository = roleRepository;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody RequestUser request ) {
        log.info("Request to login: {}", request);
        User result = userRepository.findUserByUsernameAndPassword(request.getUsername(),encodePassword(request.getPassword()));
        if(result !=  null){
            byte[] array = new byte[10];
            new Random().nextBytes(array);
            String generatedString = new String(array, Charset.forName("UTF-8"));
            Token token = tokenRepository.findTokenByUser(result);
            token.setAccessToken(encodePassword(generatedString));
            tokenRepository.save(token);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new Response<String>().builder()
                            .accessToken(token.getAccessToken())
                            .user(result.getUsername())
                            .active(result.isActive())
                            .payload(result.getRole().getName())
                            .build());
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PutMapping("/register")
    ResponseEntity<?> createUser(@Valid @RequestBody User user) throws URISyntaxException {
        log.info("Request to create user: {}", user);
        Role userRole = roleRepository.findRoleByName("USER");
        User result = userRepository.save(
                new User().builder()
                        .username(user.getUsername())
                        .password(encodePassword(user.getPassword()))
                        .active(true)
                        .role(userRole)
                        .build());
        byte[] array = new byte[10];
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        Token token = new Token().builder()
                .user(result)
                .accessToken(encodePassword(generatedString))
                .build();
        tokenRepository.save(token);
        return ResponseEntity.created(new URI("/api/user/" + result.getId()))
                .body(new Response<User>().builder()
                        .payload(result)
                        .build());
    }

    @PostMapping("/token")
    ResponseEntity<?> tokenChecker(@RequestHeader("accessToken") String accessToken)  {
        log.info("Request to verify token : {}", accessToken);
        Token result = tokenRepository.findByAccessToken(accessToken);

        if(result != null){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/update")
    ResponseEntity<?> updatePassword(@Valid @RequestBody RequestUser request,@RequestHeader("accessToken") String accessToken) {
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            log.info("Request to get user: {}", request.getUsername());
            User result = userRepository.findUserByUsernameAndPassword(request.getUsername(), encodePassword(request.getPassword()));
            if(result != null){
                result.setPassword(encodePassword(request.getNewPassword()));
                userRepository.save(result);
                return  ResponseEntity.status(HttpStatus.OK)
                        .body(new Response<String>().builder()
                                .errorMessage("Password change successfully!")
                                .build());
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new Response<String>().builder()
                            .errorMessage("Wrong password!")
                            .build());
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/revoke")
    ResponseEntity<?> revokeAccess(@Valid @RequestBody RequestUser request,@RequestHeader("accessToken") String accessToken)  {
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            User user = userRepository.findUserById(request.getId());
            if(user.isActive()){
                user.setActive(false);
            }
            else{
                user.setActive(true);
            }
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    private String encodePassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}