package com.utcn.devicemanagement.controller;

import com.utcn.devicemanagement.model.File;
import com.utcn.devicemanagement.model.Response;
import com.utcn.devicemanagement.model.Token;
import com.utcn.devicemanagement.model.User;
import com.utcn.devicemanagement.payload.UploadFileResponse;
import com.utcn.devicemanagement.repository.FileRepository;
import com.utcn.devicemanagement.repository.TokenRepository;
import com.utcn.devicemanagement.sevice.FileStorageService;
import com.utcn.devicemanagement.sevice.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class FileController {

    /**
     * FileController
     * Handle all file management needed for the application.
     * */

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    private ServiceFactory factory  = new ServiceFactory();
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private TokenRepository tokenRepository;
    private final Logger log = LoggerFactory.getLogger(FileController.class);
    @Autowired
    private FileStorageService FileStorageService; //;= (FileStorageService) factory.createService("file");


    /**
     * uploadFile take a file add save it on DB.
     * @param  file {MultipartFile} The file to be inserted in database.
     * @return UploadFileResponse {UploadFileResponse}  The path/ download link to the file.
     *
     * */
    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        File dbFile = FileStorageService.storeFile(file);
        //User user = new User("Daniel Rus","test123");
        //dbFile.addObserver(user);
        dbFile.notifyUser();
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(dbFile.getId())
                .toUriString();

        return new UploadFileResponse(dbFile.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    /**
     * downloadFile return the file given bu id.
     * @param fileId {String} The if for file requested.
     * @return The data for the given id file.
     */

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {
        // Load file from database
        File dbFile = FileStorageService.getFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }

    @GetMapping("/getFiles")
    ResponseEntity<?> getFiles(@RequestHeader("accessToken") String accessToken){
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if(token != null){
            log.info("Request to get files: {}", FileStorageService.getFiles());
            return  ResponseEntity.status(HttpStatus.OK)
                    .body(new Response<File>().builder()
                            .accessToken(token.getAccessToken())
                            .payloads(new ArrayList<>(fileRepository.findAll()))
                            .build());
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
