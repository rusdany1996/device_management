package com.utcn.devicemanagement.sevice;

import com.utcn.devicemanagement.exception.FileStorageException;
import com.utcn.devicemanagement.exception.MyFileNotFoundException;
import com.utcn.devicemanagement.model.File;
import com.utcn.devicemanagement.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

@Service
public class FileStorageService implements GenericService{

    @Autowired
    private FileRepository FileRepository;

    /**
     * Stores the file given as input in database
     *
     * @param file A multipart file
     * @return File New File generated to be store in database
     */

    public File storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            File dbFile = new File(fileName, file.getContentType(), file.getBytes());

            return FileRepository.save(dbFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    /**
     * Fetch the file with given id
     *
     *
     * @param fileId Id of the requested file
     * @return File File returned from database
     */

    public File getFile(String fileId) {
        return FileRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }

    public List<File> getFiles() {
        return FileRepository.findAll();
    }

}