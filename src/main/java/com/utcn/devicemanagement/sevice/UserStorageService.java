package com.utcn.devicemanagement.sevice;

import com.utcn.devicemanagement.exception.MyFileNotFoundException;
import com.utcn.devicemanagement.model.User;
import com.utcn.devicemanagement.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UserStorageService {

    @Autowired
    private UserRepository UserRepository;

    public User getUser(Long id) {
        return UserRepository.findById(id).
                orElseThrow(() -> new MyFileNotFoundException("File not found with id " + id));
    }
}
