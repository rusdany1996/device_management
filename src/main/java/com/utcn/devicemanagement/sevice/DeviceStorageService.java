package com.utcn.devicemanagement.sevice;

import com.utcn.devicemanagement.exception.MyFileNotFoundException;
import com.utcn.devicemanagement.model.Device;
import com.utcn.devicemanagement.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class DeviceStorageService {

    @Autowired
    private DeviceRepository DeviceRepository;

    public Device getDevice(Long id) {
        return DeviceRepository.findById(id).
                orElseThrow(() -> new MyFileNotFoundException("File not found with id " + id));
    }
}

