package com.utcn.devicemanagement.repository;



import com.utcn.devicemanagement.model.Device;
import com.utcn.devicemanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
    List<Device> findDevicesByUser(User user);
    Device findDeviceById(Long id);

}