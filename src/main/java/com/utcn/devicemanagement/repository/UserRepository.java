package com.utcn.devicemanagement.repository;

import com.utcn.devicemanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserById(Long id);
    User findUserByUsernameAndPassword(String username,String password);
    User findUserByUsername (String username);

}