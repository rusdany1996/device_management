package com.utcn.devicemanagement;

import com.utcn.devicemanagement.model.Device;
import com.utcn.devicemanagement.model.Role;
import com.utcn.devicemanagement.model.Token;
import com.utcn.devicemanagement.model.User;
import com.utcn.devicemanagement.repository.DeviceRepository;
import com.utcn.devicemanagement.repository.RoleRepository;
import com.utcn.devicemanagement.repository.TokenRepository;
import com.utcn.devicemanagement.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Random;

@Component
class Initializer implements CommandLineRunner {

    private RoleRepository roleRepository;
    private UserRepository userRepository;
    private TokenRepository tokenRepository;
    private DeviceRepository deviceRepository;

    public Initializer( RoleRepository roleRepository,UserRepository userRepository,TokenRepository tokenRepository,DeviceRepository deviceRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.deviceRepository = deviceRepository;
    }

    @Override
    public void run(String... strings) {
/*
        Role role1 = roleRepository.save(new Role().builder()
                .name("ADMIN")
                .build());
        Role role2 = roleRepository.save(new Role().builder()
                .name("USER")
                .build());
       User result = userRepository.save(new User().builder()
                .username("admin")
                .password(encodePassword("admin"))
                .active(true)
                .role(role1)
                .build());
        byte[] array = new byte[10];
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        Token token = new Token().builder()
                .user(result)
                .accessToken(encodePassword(generatedString))
                .build();
        tokenRepository.save(token);
        User result1 = userRepository.save(new User().builder()
                .username("daniel.rus@gmail.com")
                .password(encodePassword("test"))
                .active(true)
                .role(role2)
                .build());

        Device device = new Device().builder()
                .macAddress("BB-92-F8-9E-F4-94")
                .phoneCompany("Iphone 6")
                .serialNo("bpeJBrn7aTayFTzS")
                .ram("16")
                .processor("A11 Bionic")
                .user(result1)
                .build();
        deviceRepository.save(device);
        Token token1 = new Token().builder()
                .user(result1)
                .accessToken(encodePassword(generatedString))
                .build();
        tokenRepository.save(token1);

*/


    }
/*

        private String encodePassword(String password) {
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] hash = digest.digest(password.getBytes("UTF-8"));
                StringBuilder hexString = new StringBuilder();

                for (int i = 0; i < hash.length; i++) {
                    String hex = Integer.toHexString(0xff & hash[i]);
                    if (hex.length() == 1) hexString.append('0');
                    hexString.append(hex);
                }

                return hexString.toString();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

*/
}