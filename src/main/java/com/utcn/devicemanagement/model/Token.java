package com.utcn.devicemanagement.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="token")
public class Token
{
    @Id
    @GeneratedValue
    private Long id;
    @NonNull
    private String accessToken;
    @OneToOne
    @NonNull
    private User user;

}