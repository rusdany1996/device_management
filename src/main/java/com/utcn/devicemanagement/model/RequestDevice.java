package com.utcn.devicemanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class RequestDevice {

    private Long id;
    private String phoneCompany;
    private String serialNo;
    private String macAddress;
    private String ram;
    private String processor;
    private String user;
}