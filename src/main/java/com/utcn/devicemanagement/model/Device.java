package com.utcn.devicemanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Device {

    @Id
    @GeneratedValue
    private Long id;
    private String phoneCompany;
    private String serialNo;
    private String macAddress;
    private String ram;
    private String processor;
    @ManyToOne
    private User user;

}