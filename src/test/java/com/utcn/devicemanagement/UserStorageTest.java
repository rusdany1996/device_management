package com.utcn.devicemanagement;

import com.utcn.devicemanagement.model.User;
import com.utcn.devicemanagement.sevice.UserStorageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserStorageTest {


    @Mock
    private UserStorageService service;

    @Before
    public void init() {

        when(service.getUser(new Long(1))).thenReturn(User.builder().username("Daniel").password("sdasda").build());
    }

    @Test
    public void testGetFile() {
        User expectedFile = service.getUser(new Long(1));
        assertEquals("Daniel", expectedFile.getUsername());
    }
}
