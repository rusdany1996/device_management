package com.utcn.devicemanagement;

import com.utcn.devicemanagement.model.File;
import com.utcn.devicemanagement.sevice.FileStorageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static ch.qos.logback.core.encoder.ByteArrayUtil.hexStringToByteArray;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@SpringBootTest
public class FileStorageTest {

    @Mock
    private FileStorageService service;

    @Before
    public void init() {
        when(service.getFile("Image")).thenReturn(new File("Image","jpg",hexStringToByteArray("e04fd020ea3a6910a2d808002b30309d")));
    }

     @Test
     public void testGetFile() {
        File expectedFile = service.getFile("Image");
        assertEquals("jpg", expectedFile.getFileType());
    }

}
