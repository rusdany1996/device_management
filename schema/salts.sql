CREATE TABLE `Salts` (
    `id` int  NOT NULL AUTO_INCREMENT,
    `salt` varchar(255)  NOT NULL ,
    `user_id` int  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);
