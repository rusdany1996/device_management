CREATE TABLE `Tokens` (
    `id` int  NOT NULL AUTO_INCREMENT,
    `refresh_token` varchar(255)  NOT NULL ,
    `user_id` int  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);