CREATE TABLE `Files` (
    `id` varchar(255)  NOT NULL ,
    `data` longblob ,
    `file_name` varchar(255) ,
    `file_type` varchar(255) ,
    PRIMARY KEY (
        `id`
    )
);
