ALTER TABLE `Device_File` ADD CONSTRAINT `fk_Device_File_device_id` FOREIGN KEY(`device_id`)
REFERENCES `Devices` (`id`);

ALTER TABLE `Device_File` ADD CONSTRAINT `fk_Device_File_files_id` FOREIGN KEY(`files_id`)
REFERENCES `Files` (`id`);

ALTER TABLE `Salts` ADD CONSTRAINT `fk_Salts_user_id` FOREIGN KEY(`user_id`)
REFERENCES `Users` (`id`);

ALTER TABLE `Tokens` ADD CONSTRAINT `fk_Tokens_user_id` FOREIGN KEY(`user_id`)
REFERENCES `Users` (`id`);

ALTER TABLE `User_Device` ADD CONSTRAINT `fk_User_Device_user_id` FOREIGN KEY(`user_id`)
REFERENCES `Users` (`id`);

ALTER TABLE `User_Device` ADD CONSTRAINT `fk_User_Device_device_id` FOREIGN KEY(`device_id`)
REFERENCES `Devices` (`id`);
