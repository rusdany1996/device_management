CREATE TABLE `Devices` (
    `id` int  NOT NULL AUTO_INCREMENT,
    `brand` varchar(255)  NOT NULL ,
    `model` varchar(255)  NOT NULL ,
    `active` bit  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);