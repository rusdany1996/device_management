import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import {Container, Col, Form,FormGroup, Label, Input,Button } from 'reactstrap';
import './Login.css'

export class Register extends Component{

    emptyUser = {
        username : '',
        password : '',
		};
		
		_isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
          message:'',
          user: this.emptyUser,
        };
    }
    
    _onChange = (e) => {
        const { name, value} = e.currentTarget;
        let user = {...this.state.user};
        user[name] = value; 
        this.setState({
            user
        });
		}
		
		_verify(){
			if(this.state.user.username === ''){
					return false;
			}
			if(this.state.user.password === ''){
					return false;
			}
			return true;
	}

    _onSubmit = async (e) =>  {
				e.preventDefault();
				let message = "Username or Password dosen`t meet the requirements";
				if(this._verify()){
        const {user} = this.state
        await fetch('/api/register', {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(user),
        });
				this.props.history.push('/');
			}
			if(this._isMounted){
				this.setState({
					message : message
				})
			}

		}
		
	componentDidMount(){
			this._isMounted = true
	}

	componentWillUnmount(){
			this._isMounted = false
	}


    render(){
        const { _onChange, _onSubmit } = this;
        const { user } = this.state;

        return(
            
            <div className = "background">
            <Container className="App">
            <h2>Register</h2>
            <Form className="form" onSubmit={_onSubmit}>
          <Col>
            <FormGroup>
              <Label>Email</Label>
              <Input
                 name = "username"
                 value = {user.username}
                 onChange = {_onChange}
                 placeholder = "username"
                 style = {{width : 300 }}
              />
            </FormGroup>
            </Col>
            <Col>
            <FormGroup>
              <Label for="examplePassword">Password</Label>
              <Input
                type = "password"
                name = "password"
                value = {user.password}
                onChange = {_onChange}
                placeholder = "password"
                style = {{width : 300 }}
              />
            </FormGroup>
            </Col>
            <div>
            {this.state.message}
            </div>
          <Button type="submit">Sign in</Button>{"   "}
          <Button tag={Link} to="/">Cancel</Button>
        </Form>
      </Container>
      </div>
        );
    }
}
export default withRouter(Register);