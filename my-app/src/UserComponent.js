import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';
import  NavigationBar  from './NavigationBarComponent';
import logo from './logo.svg';
import './App.css';


export class User extends Component{

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

    constructor(props) {
        super(props);
        this.state = {
            request:{
              accessToken:'',
              username :''
            },
            user:{} , 
            isLoading: true
        }
    }

   async  componentDidMount() {
      const { cookies } = this.props;
      const request = {
        username : cookies.get('user')?cookies.get('user'):'',
      }
      const response = await fetch('/api/user', {
            method: 'POST',
            headers: {
                'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request),
            });
      if(response.status === 200){
        let body = await response.json();
        this.setState({user: body.payload, isLoading:false})
      }
      else{
        this.props.history.push('/')
      }
    }


    render() {
      const {user, isLoading} = this.state;
      
      if (isLoading) {
        return (
        <div className = "App">
          <header className = "App-header">
            <img src={logo} className="App-logo" alt="logo" />
          </header>
        </div>);
      }
      return (
      <div>
        <header className = "Back">
        <div>
        <NavigationBar/>
        </div>
        <div className = "back1">
        <div>
          <p>User : </p>
          <p>{user.username + " "}</p>
          </div>
          <div>
          <p>User Type : </p>
          <p>{user.role.name}</p> 
          </div>
        </div>
        </header>
        </div>
        );
      }
}
   
export default withRouter(withCookies(User));

