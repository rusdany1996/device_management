import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Col, Form,FormGroup, Label, Input,Button } from 'reactstrap';
import './App.css';
import  NavigationBar  from './NavigationBarComponent';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';


export class UpdatePassword extends Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
      };
      emptyUser = {
        oldPassword:'',
        newPassword:'',
        reNewPassword:'',
		};

    constructor(props) {
        super(props);
        this.state = {
            message:'',
            user:{
                oldPassword:'',
                newPassword:'',
                reNewPassword:'',
            },
        };
    }
    
    _onChange = (e) => {
        const { name, value} = e.currentTarget;
        let user = {...this.state.user};
        user[name] = value; 
        this.setState({
            user
        });
    }
    _verify(){
        if(this.state.user.oldPassword === ''){
            return false;
        }
        if(this.state.user.newPassword === ''){
            return false;
        }
        if(this.state.user.reNewPassword === ''){
            return false;
        }
        if(this.state.user.reNewPassword !== this.state.user.newPassword ){
            return false;
        }
        return true;
    }

    _onSubmit = async (e) =>  {
        e.preventDefault();
        const {user} = this.state;
        const { cookies } = this.props;
        const request = {
            username : cookies.get('user'),
            password : user.oldPassword,
            newPassword : user.newPassword, 
        }
        if(this._verify()){
        const response = await fetch('/api/update', {
                method: 'POST',
                headers: {
                    'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(request),
                });

            if(response.status !== 401){
                let body = await response.json();
                this.setState({
                    message : body.errorMessage,
                    user : this.emptyUser
                });
            }else{
                this.props.history.push('/')
            }
        }
        else{
            this.setState({
                message :' Passwords dosen`t match!'
            });
        }   
    }

    render(){
        const { _onChange, _onSubmit } = this;
        const { user } = this.state;

        return(

           
            <div className = "background">
             <NavigationBar/>
            <Container className="App">
            <h2>Update Password</h2>
            <Form className="form" onSubmit={_onSubmit}>
          <Col>
            <FormGroup>
              <Label>Old Password</Label>
              <Input
                 name = "oldPassword"
                 type = "password"
                 value = {user.oldPassword}
                 onChange = {_onChange}
                 placeholder = "OldPassword"
                 style = {{width : 300 }}
              />
            </FormGroup>
            </Col>
            <Col>
            <FormGroup>
              <Label>New Password</Label>
              <Input
                type = "password"
                name = "newPassword"
                value = {user.newPassword}
                onChange = {_onChange}
                placeholder = "NewPassword"
                style = {{width : 300 }}
              />
            </FormGroup>
            </Col>
            <Col>
            <FormGroup>
              <Label>Rewrite New Password</Label>
              <Input
                type = "password"
                name = "reNewPassword"
                value = {user.reNewPassword}
                onChange = {_onChange}
                placeholder = "RewriteNewPassword"
                style = {{width : 300 }}
              />
            </FormGroup>
            </Col>
            <div>
            {this.state.message}
            </div>
          <Button type="submit">Change</Button>
        </Form>
      </Container>
      </div>
        );
    }
}


export default withRouter(withCookies(UpdatePassword));
