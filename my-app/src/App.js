import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';
import Register from './RegisterComponent';
import Users from './UsersComponent'
import Home from './HomeComponent'
import Login from './LoginComponent'
import User from './UserComponent'
import Devices from './DevicesComponent'
import Device from './DeviceComponent'
import Files from './FilesComponent'
import UpdatePassword  from './UpdatePasswordComponment'
import FileUpload from './FileUploadComponent'

export class App extends Component { 
  
  render() {
    return (
      <CookiesProvider>
          <Router>
            <Switch>
              <Route path = '/' exact={true} component = {Login}/>
              <Route path = '/register' exact={true} component = {Register}/>
              <Route path = '/home' exact={true} component = {Home}/>
              <Route path = '/users' exact={true} component = {Users}/>
              <Route path = '/files' exact={true} component = {Files}/>
              <Route path='/user' component={User}/>
              <Route path = '/updatepwd' exact={true} component = {UpdatePassword}/>
              <Route path = '/devices' exact={true} component = {Devices}/>
              <Route path='/device/:id' component={Device}/>
              <Route path='/upload' component={FileUpload}/>
            </Switch>
          </Router>
        </CookiesProvider>
    );
  }
}

export default App;
