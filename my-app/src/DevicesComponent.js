import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { Link } from 'react-router-dom';
import { withCookies ,Cookies} from 'react-cookie';
import {Table, Button} from 'reactstrap';
import  NavigationBar  from './NavigationBarComponent';
import logo from './logo.svg';
import './App.css';

export class Devices extends Component{

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

    constructor(props) {
        super(props);
        this.state = {
            devices: [], 
            isLoading: true
        }
    }


    async componentDidMount() {
      const { cookies } = this.props;
        const response = await fetch('/api/devices', {
            method: 'GET',
            headers: {
                'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                'user': cookies.get('user'),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            });
        if(response.status === 200){
          let body = await response.json();
          this.setState({devices: body.payloads , isLoading : false});
        }
        else{
          this.props.history.push('/')
        }
    } 



    render() {
      const {devices, isLoading} = this.state;
      const { cookies } = this.props;
      const elemetDevices = devices.map(device =>  
      <tr key={device.id}>
        <th scope="row">{device.id}</th>
        <td>{device.phoneCompany}</td>
        <td>{device.serialNo}</td>
        <td>{device.macAddress}</td>
        <td>{device.ram}</td>
        <td>{device.processor}</td>
        {cookies.get('userType')=== 'ADMIN'?<td>{device.user !== null ?device.user.username:''}</td>:null}
        <td><Button size="sm" color="primary" tag={Link} to={"/device/" + device.id}>Edit</Button></td>
      </tr>
      );

      if (isLoading) {
        return (
        <div className = "App1">
          <header className = "App-header">
            <img src={logo} className="App-logo" alt="logo" />
          </header>
        </div>);
      }

      return (
      <div>
        <header className = "Back">
          <div>
            <NavigationBar/>
          </div>
          <div className = "text-right">
            <Button color="primary" className = "spatire" tag={Link} to= "/device/new">Add Device</Button>
          </div>
          <div>
            <Table dark>
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Brand</th>
                  <th>Serial Number</th>
                  <th>Mac Address</th>
                  <th>Ram</th>
                  <th>Processor</th>    
                  {cookies.get('userType')=== 'ADMIN'?<th>User</th>:null}
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {elemetDevices}
              </tbody>
            </Table>
            </div>
          </header>
        </div>
        );
      }
}
   
export default withRouter(withCookies(Devices));

