import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';
import  NavigationBar  from './NavigationBarComponent';
import {Table, Button} from 'reactstrap';
import { Link } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

export class Files extends Component{
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
      };
    
        constructor(props) {
            super(props);
            this.state = {
                files: [], 
                isLoading: true
            }
        }
        async componentDidMount() {
            const { cookies } = this.props;
              const response = await fetch('/api/getFiles', {
                  method: 'GET',
                  headers: {
                      'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                  },
                  });
                  if(response.status === 200){
                    let body = await response.json();
                    this.setState({files: body.payloads , isLoading : false});
                  }
          } 
          
          downloadFile = async(id) => {
            const { cookies } = this.props;
            const response = await fetch('/api/downloadFile/'+id, {
                method: 'GET',
                headers: {
                    'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                });
                if(response.status === 200){
                  window.open(response.url);
            }
        }

      
          render() {
            const {files, isLoading} = this.state;
            const {downloadFile} = this;
            const elemetFiles = files.map(file =>  
            <tr key={file.id}>
              <th scope="row">{file.id}</th>
              <td>{file.fileName}</td>
              <td>{file.fileType}</td>
              <td><Button size="sm" color="primary" onClick = {() => downloadFile(file.id)}>Download</Button></td>
            </tr>
            );
            if (isLoading) {
              return (
              <div className = "App1">
                <header className = "App-header">
                  <img src={logo} className="App-logo" alt="logo" />
                </header>
              </div>);
            }
            return (
            <div>
              <header className = "Back">
              <div>
              <NavigationBar/>
              </div>
              <div className = "text-right">
            <Button color="primary" className = "spatire" tag={Link} to= "/upload">Upload File</Button>
             </div>
              <div>
                <Table dark>
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  {elemetFiles}
                  </tbody>
                </Table>
              </div>
              </header>
              </div>
              );
            }
      }
         
      export default withRouter(withCookies(Files));
      
      