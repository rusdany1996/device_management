import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import  NavigationBar  from './NavigationBarComponent';


class FileUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
      file:null
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fileUpload = this.fileUpload.bind(this)
  }
  onFormSubmit(e){
    e.preventDefault()
    this.fileUpload(this.state.file).then((response)=>{
      this.props.history.push('/files');
    })
  }

  onChange(e) {
    this.setState({file:e.target.files[0]})
  }

  fileUpload(file){
    const url = '/api/uploadFile';
    const formData = new FormData();
    formData.append('file',file)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return  axios.post(url, formData,config)
  }

  render() {
    return (
        <div>
        <header className = "Back">
        <div>
          <NavigationBar/>
        </div>
      <form onSubmit={this.onFormSubmit}>
        <div className='padding-file'>
        <h3>File Upload</h3>
        </div>
        <div className='padding-form'>
        <input type="file" onChange={this.onChange} />
        <button type="submit">Upload</button>
        </div>
      </form>
      </header>
      </div>
   )
  }
}



export default withRouter(FileUpload)