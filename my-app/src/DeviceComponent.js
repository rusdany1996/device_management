import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';
import  NavigationBar  from './NavigationBarComponent';
import {Button, Container, Form, FormGroup, Input, Label, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import './App.css';
import { Link } from 'react-router-dom';


export class Device extends Component{

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  emptyDevice = {
    id:'',
    phoneCompany: '',
    serialNo : '',
    macAddress: '',
    ram: '',
    processor: '',
    user: {
      username:'',
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      device: this.emptyDevice,
      isOpen: false,
      users:[]
    };
  }

  toggle= () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  async componentDidMount() {
    const { cookies } = this.props;
    if (this.props.match.params.id !== 'new') {
      const response = await fetch(`/api/device/${this.props.match.params.id}`, {
        method: 'GET',
        headers: {
            'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        });
      if(response.status === 200){
        let body = await response.json();
        this.setState({device: body});
      }
      else{
        this.props.history.push('/')
      }
    }

    const response = await fetch(`/api/users`, {
      method: 'GET',
      headers: {
          'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      });
    if(response.status === 200){
      let body = await response.json();
      this.setState({users: body.payloads});
    }
    else{
      this.props.history.push('/')
    }


  }

  _onChange = (e) =>  {
    const { name, value} = e.currentTarget;
    let device = {...this.state.device};
    device[name] = value;
    this.setState({device});
  }

  _onChangeUser = (user) => {
    let device = {...this.state.device};
    let device1 = Object.assign({}, device,{ user :{username: user}});
    this.setState({device :device1});
  }

  _onSubmit = async (e) => {
    e.preventDefault();
    const { cookies } = this.props;
    const {device} = this.state;
    const request = {
      id: device.id,
      phoneCompany: device.phoneCompany,
      serialNo : device.serialNo,
      macAddress: device.macAddress,
      ram: device.ram,
      processor: device.processor,
      user :cookies.get('userType') === 'ADMIN'? device.user.username:cookies.get('user'),
    }
    await fetch('/api/device', {
      method: (device.id) ? 'PUT' : 'POST',
      headers: {
        'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(request),
    });
    this.props.history.push('/devices');
  }

  render() {
    const { device, isOpen,users } = this.state;
    const { cookies } = this.props;
    const {_onSubmit , _onChange, _onChangeUser, toggle} = this;
    const title = <h2>Edit Device</h2>;
    const elemetDevices = users.map(user =>  
      <DropdownItem key={user.id}  onClick = {( ) =>_onChangeUser(user.username)}>{user.username}</DropdownItem>
      );
    return <div className ="Back">
      <NavigationBar/>
      <Container>
        {title}
        <Form onSubmit={_onSubmit}>
          <FormGroup>
            <Label for="Brand">Brand</Label>
            <Input type="text" name="phoneCompany" id="phoneCompany" value={device.phoneCompany || ''}
                   onChange={_onChange} autoComplete="phoneCompany"/>
          </FormGroup>
          <FormGroup>
            <Label for="serialNo">Serial Number</Label>
            <Input type="text" name="serialNo" id="serialNo" value={device.serialNo || ''}
                   onChange={_onChange} autoComplete="serialNo"/>
          </FormGroup>
          <FormGroup>
            <Label for="macAddress">Mac Address</Label>
            <Input type="text" name="macAddress" id="macAddress" value={device.macAddress || ''}
                   onChange={_onChange} autoComplete="macAddress"/>
          </FormGroup>
          <div className="row">
            <FormGroup className="col-md-4 mb-3">
              <Label for="ram">Ram</Label>
              <Input type="text" name="ram" id="ram" value={device.ram || ''}
                     onChange={_onChange} autoComplete="ram"/>
            </FormGroup>
            <FormGroup className="col-md-5 mb-3">
              <Label for="processor">Processor</Label>
              <Input type="text" name="processor" id="processor" value={device.processor || ''}
                     onChange={_onChange} autoComplete="processor"/>
            </FormGroup>
            {cookies.get('userType') === 'ADMIN'?
            <FormGroup className="col-md-3 mb-3">
              <Label for="user">User</Label>
              <div>
              <ButtonDropdown isOpen={isOpen} toggle={toggle}>
                    <DropdownToggle caret>
                      {device.user !== null ? device.user.username : ''}
                    </DropdownToggle>
                    <DropdownMenu>
                    {elemetDevices}
                    </DropdownMenu>
                  </ButtonDropdown>
                  </div>
            </FormGroup>:null
          }
          </div>
          <FormGroup>
            <Button color="primary" type="submit">Save</Button>{' '}
            <Button color="secondary" tag={Link} to="/devices">Cancel</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}
   
export default withRouter(withCookies(Device));

