import React, { Component } from 'react';
import './App.css';
import NavigationBar from './NavigationBarComponent';
import { withRouter} from 'react-router-dom';
import { } from 'reactstrap';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';


export class Home extends Component {
  
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };
  
  async componentDidMount(){
    const { cookies } = this.props;
    const response = await fetch('/api/token', {
      method: 'POST',
      headers: {
          'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):"",
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      })
      if(response.status !== 200){
        this.props.history.push('/')
      }
    
  }

  render() {
    return (
      <div>
        <NavigationBar/>
        <div className = "App1">
          <header className="App-header">
            <p>
               Remote Device Management
            </p>
          </header>
        </div>
      </div>
    );
  }
}


export default withRouter(withCookies(Home));